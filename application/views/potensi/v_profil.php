<style media="screen">
.table-bordered{
  border-right: 3px solid black:
}

#profil p{
  text-indent: 40px;
  font-weight: 350;
}
</style>
<section id="get-started" class="padd-section text-center wow fadeInUp">
  <div class="containers">
    <div class="section-title text-center">
      <h2>Profil Desa Karangasem</h2>
      <!-- <p class="separator" style="">Berikut data golongan darah warga Desa Karangasem.</p> -->
    </div>
    <div class="row">
      <div class="col-md-12 mt-5 text-left" id="profil">
        <h3>Visi</h3>
        <p class="lead text-center" style="text-transform:capitalize;">“TERWUJUDNYA TATA KELOLA PEMERINTAHAN YANG BAIK, TRANSPARAN DAN JUJUR GUNA MEWUJUDKAN KEHIDUPAN MASYARAKAT YANG ADIL, MAKMUR DAN SEJAHTERA” </p>
        <p class="text-justify">Aparatur Desa Karangasem dan Masyarakat setempat sepakat bahwa Visi adalah gambaran umum dari kondisi yang ideal yang dibutuhkan oleh Desa Karangasem di masa yang akan datang yang dicapai bersama dengan partisipasi masyarakat untuk jangka waktu tertentu.</p>
        <p class="text-justify">Jangka waktu sebagaimana dimaksud sesuai dengan Peraturan Menteri dalam Negeri Nomor 114 tahun 2014 tentang Pembangunan Desa, yaitu 6 (enam) tahun. Jangka waktu dimaksud bagi Desa Karangasem adalah dari Tahun 2021 sampai dengan  2027.</p>
        <br>
        <h3>Misi</h3>
        <p class="text-justify">Adapun misi Desa Karangasem adalah :</p>
        <ul class="">
          <li>Melakukan Rrenovasi sistem kinerja pemerintahan desa guna meningkatkan kualitas pelayanan kepada masyarakat dengan cepat, tepat dan benar.</li>
          <li>Terwujudnya tata kelola pemerintahan yang baik, transparan dan jujur guna mewujudkan kehidupan masyarakat yang adil dan makmur.</li>
          <li>Melanjutkan/meningkatkan pembangunan serta mengembangkan sarana umum, pendidikan dan keagamaan. </li>
          <li>Memberdayakan lembaga yang ada dan mengoptimalkan kegiatan pemuda dan olahraga guna menekan kenakalan remaja.</li>
          <li>Menciptakan Kondisi masyarakat Desa Karangasem yang aman, tertib dan rukun sehingga menjadi desa yang maju dan mandiri.</li>
          <li>Mewujudkan dan meningkatkan ketertiban dan keamanan bagi desa.</li>
        </ul><br>
        <!-- <div class="col-md-12 ml-3"> -->
        <h3>Susunan Organisasi dan Tata Kerja Pemerintah Desa Karangasem</h3><br>
        <img src="<?=base_url()?>assets/img/STRUKTUR KADUS.png" alt="" style="width:100%;" id="imgsusunan"><br><br>
        <h3>Jumlah Penduduk</h3>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th scope="col">Jumlah Laki-laki</th>
              <th scope="col">Jumlah Perempuan</th>
              <th scope="col">Jumlah Penduduk</th>
              <th scope="col">Jumlah KK</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>2816</td>
              <td>2843</td>
              <td>5659</td>
              <td>2042</td>
            </tr>
          </tbody>
        </table>
        <!-- </div> -->
        <br>
        <h3>Sejarah</h3>
        <p class="text-justify">Pada jaman dahulu kala , belum ada jalan kereta api dan jalan beraspal juga belum ada aliran irigasi serta penerangan listrik. Pada pertengahan abad ke XV  Masehi yang ditandai dengan Tahun SANGKALA yang berbunyi “ SIRNA ILANG KERTANING BUMI “, Di tanah Karangasem ini masih merupakan tanah hutan belukar berduri dan  angker, serta banyak pohon asam besar , gombang- gombang, kolam dan sungai yang penuh lumpur encer ( embel ). Maka dari itu banyak terdapat kura-kura yang sebesar nyiru dan masih banyak buaya yang naik kedaratan untuk memanaskan badanya. Juga binatang liar dan buas, seperti macan, babi hutan, kijang atau menjangan, monyet, ular besar, ayam hutan dan burung hantu yang jika malam mengeluarkan bunyi yang menakutkan dan membuat bulu kuduk berdiri.
          Hutan di sini dari sebelah selatan seolah-olah merupakan kepanjangan dari kaki Gunung Ciremai dan rentetannya dengan Gunung Kromong dan Gunung Guo Dalem. Menurut cerita, hutan disini tempat bersemayamnya raja-raja lelembut yang dipimpin oleh Maha Braja. Hutan tersebut masih kelihatan angker dan misterius, sebab pada jaman dahulu yang menguasainya ialah bangsa jin, setan, peri merkayangan. Maka siapa saja yang berani memasuki hutan tersebut pasti akan menemui ajalnya. Sampai sekarangpun kadang-kadang orang masih menjumpai hal-hal yang gaib, kiranya orang tersebut masih mau menengok bekas daerah kekuasaanya pada jaman dahulu kala. Yang ada pada waktu itu hanya ada jalan setapak, maka dari itu tidak ada orang yang berani lewat melalui hutan itu.
          Pada suatu waktu di daerah hutan tersebut itu kelihatan seorang laki-laki satria, orangnya tenang, tubuhnya kekar, dan kalau bicara seperlunya saja. Orang itu biasanya disebut Ki T. Manggis atau Ki Danalampa namanya. Adapun rekan-rekanya yang bernama Ki Werdi, Ki Kelipa, Ki Japar, Ki Gondea, Ki Sarif, Ki Jaya Lelana, Ki Lasem atau Nyi Lasem dan masih banyak lagi teman-temanya. Pohon-pohon jati dan asem yang besar-besar dipotong dan ditebang.
          Setelah hutan tersebut menjadi tanah lapang dan keadaan aman, tentram selanjutnya Ki Dana Lampa, Ki Werdi, Ki Kelipah, Ki Japar, Ki Gondea, Ki Salem dan Ki Sarif bersama-sama mengatur tanah tersebut untuk pesawahan, pekarangan, jalan-jalan, irigasi dan sebagainya. Sesudah itu mencari bibit-bibit padi, gandum, jewawut, pisang, pepaya, mangga dan sebagainya.
          Lama-kelamaan penduduk Ki Danalampa dan Ki Werdi atau baik dari orang maupun dari tetangga pendudukan dan dari rakyatnya sendiri mendapatkan sebuah nama. Pedukuan Gombang dan ada juga yang menyebutnya Pedukuan Karangasem, sebab tanah hutan yang digarap dulu sebelah selatan banyak lubuk atau kedung Gombangnya dan sebelah utara banyak pohon asem .
          Para utusan tadi tujuh diantaranya berasal dari tanah Galuh Pajajaran yang kesemuanya sudah menguasai ilmu lahir dan batin yang sempurna . Lebih-lebih Ki Danalampa mempunyai arti dan falsafah terdiri dari dana artinya pemberian lampah artinya laku atau kelakuan,mengandung lambang selalu bersedia mendarmabaktikan perbuatanya segala waktu.
          Ki Werdi,orangnya hitam manis , tubuhnya tegak, pandai dan juling atau lihai. Bicaranya tegas tidak pernah main-main atau bergurau dalam bicara,tetapi juga wajahnya kelihatan sadis tetapi sopan. Kalau berpakaian  senang pada yang serba hitam , ikat mantokan model tutup liwelan untuk menutupi rambutnya yang panjang mengurai . Falsafah Ki Werdi mengandung arti tata titi , berhati-hati,teliti dan terlaksana segala yang direncanakan dengan sempurna.
          Ki Jaya Lelana , orangnya tinggi besar gagah perkasa,berkumis , berjambang dan berbulu dada lebat. Sakti mandra guna kalau bicara seperlunya saja tetapi sekali berbicara dan memerintah sambil membelalakkan matanya , sehingga yang diperintah sambil gemetaran terus berangkat , dan ia mempunyai anak buah mahkluk yang disayangi yaitu Ki Punuk , Ki Salip , Ki Ridung dan Ki wunut.
          Ki Kelipah , orangnya kalem dan tenang , sedikit bicara dan banyak bekerja. Sekali bicara yang sifatnya mengutuk langsung ada buktinya maka semua rekan dan bawahanya sangat menyayangi dan menghormatinya .
          Ki Japar , orangnya gagah perkasa, pendek kekar , berkumis dan berjambang tebal bersambungan dengan bulu dadanya . kalau bicara suaranya agak besar . Ki Japar adalah orang yang gagah perwira dan sakti mandraguna juga bekas perwira pajajaran pilih tanding, pandai mengatur siasat perang dan mempunyai ajian dan ilmu dari leluhur pajajaran berupa pusaka batu kramat yang kalau di gosok- gosok dapat berubah bentuk seperti macan siliwangi yang dapat mengeluarkan suara raungan yang menakutkan .
          Ki Lasem dan Nyi Lasem  bekas babu dan pesuruhan kemit kraton pajajaran yang mempunyai kesaktian ilmu merkayangan .
          Pada pertemuan itu Ki werdi dan Ki Danalampa menyerahkan hasil karyanya kepada pegusten dalem Cerbon agar daerah baru itu dapat di terima sebagai daerah wilayah Cirebon yang baru dan kedua daerah tersebut minta di beri nama .
          Dengan kewaspadaan dan penuh kebijaksanaan dan atas dasar bahan-bahan yang telah di dapat maka pegusten dalam Cerbon memberi nama pedukuan Gombang untuk pedukuanya Ki Danalampa , pedukahan Karang asem untuk pedukuan Ki Werdi .
          Kedua nama tersebut dapat di terima dan di setujui oleh semua hadirin , karena kedua nama tersebut telah membawa makna falsafah sejarah sendiri .setelah di setujui nama-nama pedukuhan itu , pegusten dalem Cerbon lalu menunjuk Ki Danalampa sebagai kepala pedukuan Gombang dan Ki werdi sebagai kepala pedukuan Karangasem .
          Sebelum pulang pegusten Cerbon berpesan kepada Ki Werdi agar kepada para pembantunya supaya di beri pembagian tanah untuk di pergunakan sebagai peninggalan kepada anak cucunya di kelak kemudian hari .  
          Berdasarkan pesan dan nasehat dari pegusten maka kepada yang telah ikut berjasa kepada pedukuan ,seperti Ki Kelipah di angkat sebagai penasehat Ki werdi di beri pembagian tanah di blok Setingal .pada jaman dulunya setiap minggu sekali suka kelihatan ada drajat atau bronggol Ki Japar . Ki Japar  juga di beri  pembagian tanah di blok Siwalan .dan blok Siwalan pada jaman itu di pakai tempat atau kantor penyimpanan pusaka–pusaka,   Ki Werdi . Ki Gondea di beri pembagian tanah di blok Buyut haji ,sampai wafatnya juga di kubur di pekuburan ki buyut haji . Ki Sarip yang memiki pusaka batu jika di gosok-gosok dapat berubah menjadi macan Siliwangi , di beri pembagian dan sampai wafatnya beliau di kubur di atas tanah hasil jerih payahnya sendiri yang di sebut kuburan Ki buyut Sarip . Ki Lasem dan Nyi Lasem mendapat bagian di blok si kramat sampai wafatnya di kubur di pekuburan si Kramat . Ki jaya Lelana yang punya pesuruh orang Ki Punuk ,Ki Wunut , Ki Ridung yang menjaga komplek jembatan karangasem Plumbon dan Ki Sarip di Pekulen dan masih jrahil atau masih sering mengganggu . beliau juga di beri tempat wafatnya di pekuburan Jaya Lelana . Ki Werdi adalah salah satu pahlawan kerja dan ahli mengatasi persoalan dalam terang maupun gelap beliau wafat di kuburkan di pekuburan Karangasem blok Sinten selanjutnya di sebut Buyut Karangasem sebagai nenek moyang bumi Karangasem sampai sekarang . di Karangasem ,Pekantingan dan Peningkiran juga ada bekas-bekas peninggalan buyut Karangasem .
          Bumi berputar ,musim beredar , hari berganti minggu ,minggu menjadi bulan dan bulan berubah menjadi tahun , tahun menjadi windu dan sebagainya .  Desa karangasem di bawa asuhan pemerintah kesultanan Cirebon , selanjutnya pada tahun 1825 Masehi pada waktu Gubernur Jendral Betawi Tuan De Kock  namanya , pada waktu itu akan membuat jalan raya dari Banten sampai Banyuwangi yang menimbulkan terjadinya perang di Ponegoro .sebab Keraton P. Di Ponegoro di pasang tongkat merah yang di sebut jalan Daendels .</p>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
new Chart(document.getElementById("bar-chart"), {
  type: 'bar',
  data: {
    labels: ["A", "B", "AB", "O","Tidak Tahu"],
    // labels: ["A", "B", "AB", "O", "A-", "B-", "AB-", "O-", "A+", "B+", "AB+", "O+","Belum Diketahui"],
    datasets: [
      {
        // label: "Population (millions)",
        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
        // backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850","red","teal","skyblue","lime","blue","yellow","green","maroon"],
        data: [2478,5267,734,784,433]
        // data: [2478,5267,734,784,433,290,120,175,734,784,433,290,111]
      }
    ]
  },
  options: {
    legend: { display: false },
    title: {
      display: true,
      text: 'Total Penduduk : 6651'
    }
  }
});
</script>









<!-- <?php //echo form_open_multipart(base_url("surat/isi"),array('class' => 'form-horizontal')); ?>
<textarea name="isiartikel" id="ckeditor" class="ckeditor" rows="8" cols="80"></textarea>
<input type="submit" name="gas" value="Gas">
<?php //echo form_close(); ?>
<script type="text/javascript">
CKEDITOR.disableAutoInline = true;
CKEDITOR.inline = editable;
</script> -->
